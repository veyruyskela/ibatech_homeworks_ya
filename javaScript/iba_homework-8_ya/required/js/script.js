let mainForm = document.createElement("form");
mainForm.setAttribute("action", "#");
let inputField = document.createElement("input");
inputField.setAttribute("type", "number");
inputField.setAttribute("id", "inputField");
let inputFieldLabel = document.createElement("label");
inputFieldLabel.setAttribute("for", "inputField");
inputFieldLabel.innerHTML = "Price";
mainForm.appendChild(inputFieldLabel);
mainForm.appendChild(inputField);
let firstScript = document.querySelector("script");
firstScript.before(mainForm);
let closeButton = document.createElement("button");
closeButton.innerHTML = "x";
let currentPrice = document.createElement("span");
let errorMessage = document.createElement("span");

inputField.addEventListener("focus", (event) => {
  inputField.style = "outline: 1px solid green;color:green;";
});

inputField.addEventListener("focusout", (event) => {
  inputField.style = "outline: 1px solid transparent;";
  if ( inputField.value < 0 ) {
    inputField.style = "outline: 1px solid red;";
    mainForm.appendChild(errorMessage);
    errorMessage.innerHTML = "Please enter correct price";
  } else {
    currentPrice.innerHTML = `Current price: ${inputField.value}`;
    mainForm.appendChild(currentPrice);
    mainForm.appendChild(closeButton);
  }
});

closeButton.addEventListener("click", (event) => {
  currentPrice.remove();
  closeButton.remove();
  inputField.value = 0;
});