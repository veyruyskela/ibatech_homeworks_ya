let m = +prompt( "Enter the first number" );
/* checking if the first number is integer number */
while ( !Number.isInteger(m) ) {
	m = +prompt( "Enter the first number" );
}

let n = +prompt( "Enter the second number" );
/* checking if the second number is integer number */
while ( !Number.isInteger(n) ) {
	n = +prompt( "Enter the second number" );
}
/* the main functionality */
if ( m >= n ) {
	alert( "ERROR: First number should be less than second number" );
} else {
	for ( let i = m; i <= n; i++ ) {
		for ( let j = 2; j < i; j++ ) {
			if ( i % j === 0 ) {
				break; /* this number is composite */
			}
			if ( j === i - 1 ) {
				console.log(i); /* and this one is prime */
			}
		}
	}
}
