A method of an object is a function that can ba executed by the object.
Also, method is some kind of property of the object.
As an example: if we have a `dog` object, we can create `barking` object so `dog` can bark.