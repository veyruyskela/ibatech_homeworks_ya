The `let` is a modern method to declare the variable in JS, while the `var` is out-of-date one. The difference between them is a visibility area (scope).
`let` is visible inside a certain block such as `if` statement or a loop,
whereas `let` is a global variable and can be reached anywhere in the code except functions.

The `const` is the same as the `let`, but there is an arrangement that the value of `const` cannot be changed. The role of `const` is to behave itself as a constant value.

The using of `var` is undesirable because the declaration of `var` starts at the same time as the script (or a function with `var` inside it) starts to execute.