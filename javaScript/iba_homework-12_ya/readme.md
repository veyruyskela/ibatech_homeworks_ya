1. `setTimeout()` allows us to specify the time for execution of a function in seconds after the function executes normally (without `setTimeout()`)
`setInterval()` is used to call the function several times with a certain periodicity.
2. Instead of instantly execution, function will execute as fast as it is possible. It is beacause of previous commands/functions should run first.
3. Forgetting to call the `clearInterval()` may cause infinite running the function.