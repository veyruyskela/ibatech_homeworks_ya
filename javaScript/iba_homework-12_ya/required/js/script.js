let stopButton = document.createElement("button");
stopButton.innerHTML = "Stop";
let resumeButton = document.createElement("button");
resumeButton.innerHTML = "Resume";
let firstScript = document.querySelector("script");
firstScript.before(stopButton, resumeButton);

let images = document.querySelectorAll(".image-to-show");

function hideAllImages() {
  for ( let i = 0; i < images.length; i++ ) {
    images[i].hidden = true;
  }
}

function showImage(i) {
  images[i].hidden = false;
}

let delay = 0;

function banner() {
  for ( let i = 0; i < images.length; i++ ) {
    let myTimer = setTimeout(main, delay, i);
    delay += 1000;

    stopButton.addEventListener("click", function() {
         clearTimeout(myTimer);
      });
      
        resumeButton.addEventListener("click", function() {
            
      });
  }
}

function main (i) {
  hideAllImages();
  showImage(i);
}

setInterval(banner, 0);

/*banner();*/