let themeChanger = document.querySelector(".themeChanger");

if ( localStorage.getItem("isdarkTheme") === "false" )  {
	changeThemeToLight();
	localStorage.setItem("isdarkTheme", false);
}

themeChanger.addEventListener("click", function(){
	if ( localStorage.getItem("isdarkTheme") === "false" ) {
		changeThemeToDark();
		localStorage.setItem("isdarkTheme", true);
	} else if ( localStorage.getItem("isdarkTheme") === "false" || typeof(localStorage.getItem("isdarkTheme")) !== String )  {
		changeThemeToLight();
		localStorage.setItem("isdarkTheme", false);
	}
});

function changeThemeToLight() {
	document.querySelector("body").style = "background-color:gold;color:black";
}
function changeThemeToDark() {
	document.querySelector("body").style = "background-color:black;color:gold";
}