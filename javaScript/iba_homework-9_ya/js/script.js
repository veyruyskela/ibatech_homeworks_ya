let tabs = document.querySelectorAll(".tabs-title");
let tabsContent = document.querySelectorAll(".tabs-content > li");

for ( let i = 0; i < tabs.length; i++ ) {
  tabs[i].addEventListener("click", (event) => {
    for ( let j = 0; j < tabs.length; j++ ) {
      tabs[j].classList.remove("active");
      tabsContent[j].style = "display:none;";
    }
    tabs[i].classList.add("active");
    tabsContent[i].style = "display:list-item;";
  })
}