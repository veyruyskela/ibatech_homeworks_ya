$(".main-menu-nav-link").on("click", function(){
	let target = $(this).attr("href").split("#")[1];
	$('html, body').animate({
		scrollTop: $(`a[name='${target}']`).offset().top
	},{
		duration: 1000,
	});
});

$("script:first").before("<button class='toTopButton'>to top</button>");
$(".toTopButton").css("position", "fixed");
$(".toTopButton").css("bottom", "0");
$(".toTopButton").css("right", "0");
$(".toTopButton").css("width", "100px");
$(".toTopButton").css("height", "100px");
$(".toTopButton").css("display", "none");

$(document).scroll(function(e){
	if ( $(window).scrollTop() > 480 ) {
		$(".toTopButton").css("display", "inline-block");
	}
	if ( $(window).scrollTop() === 0 ) {
		$(".toTopButton").css("display", "none");
	}
});

$(".toTopButton").on('click', function() {
	$('html, body').animate({scrollTop:0}, '1000');
});


let hidden = false;

$(".hideThis").click(function(){
  $(".latest-imgs").slideToggle();
  if ( hidden == false) {
  	$(".hideThis").text("Show this");
  } else {
  	$(".hideThis").text("Hide this");
  }
  hidden = !hidden;
});