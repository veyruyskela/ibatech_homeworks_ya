function filterBy(array, dataType) {
	let filteredArray = [];
	array.forEach( function filterArray(element) {
		if ( typeof(element) !== dataType ) {
			filteredArray.push(element);
		}
	});
	return filteredArray;
}

let myArray = ['hello', 'world', 23, '23', null];

console.log( filterBy(myArray, "string") );
//console.log( filterBy(myArray, "number") );
//console.log( filterBy(myArray, "object") );
