let buttons = document.querySelectorAll(".btn");

function recolor(buttonName) {
  for ( let i = 0; i < buttons.length; i++ ) {
    buttons[i].style = "background-color:#33333a;";
    if ( buttons[i].innerHTML === buttonName ) {
      buttons[i].style = "background-color:#3355dd;"
    }
  }
}

document.addEventListener("keyup", (event) => {
  if ( event.code == "Enter" ) {
    recolor("Enter");
  } else if ( event.code === "KeyS" ) {
    recolor("S");
  } else if ( event.code === "KeyE" ) {
    recolor("E");
  } else if ( event.code === "KeyO" ) {
    recolor("O");
  } else if ( event.code === "KeyN" ) {
    recolor("N");
  } else if ( event.code === "KeyL" ) {
    recolor("L");
  } else if ( event.code === "KeyZ" ) {
    recolor("Z");
  }
});