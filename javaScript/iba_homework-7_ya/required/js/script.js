function displayList (array) {
	let firstScript = document.querySelector("script");
	let myList = document.createElement("ul");
	firstScript.before(myList);

	let myMap = array.map( function(e) {
		return e;
	});

	/*
	for ( let i = 0; i < myMap.length; i++ ) {
		myList.appendChild( document.createElement("li") ).textContent = myMap[i];
	}
	*/

	let listItems = "";

	for ( let i = 0; i < myMap.length; i++ ) {
		listItems += `<li>${myMap[i]}</li>\n`;
	}

	console.log(listItems);
	myList.innerHTML = listItems;
}

displayList( ["a", "b", "c"] );
//displayList( ['hello', 'world', 'Baku', 'IBA Tech Academy', '2019'] );
//displayList( ['1', '2', '3', 'sea', 'user', 23] );
