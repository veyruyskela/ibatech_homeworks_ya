let iconEyes = document.querySelectorAll(".icon-password");
let inputPassword = document.querySelectorAll("input");

for ( let i = 0; i < iconEyes.length; i++ ) {
	iconEyes[i].addEventListener("click", (event) => {
		if ( inputPassword[i].getAttribute("type") === "password" ) {
			inputPassword[i].setAttribute("type", "text");
			iconEyes[i].classList.remove("fa-eye-slash");
			iconEyes[i].classList.add("fa-eye");
		} else if ( inputPassword[i].getAttribute("type") === "text" ) {
			inputPassword[i].setAttribute("type", "password");
			iconEyes[i].classList.remove("fa-eye");
			iconEyes[i].classList.add("fa-eye-slash");
		}
	});
}

let errorMessage = document.createElement("span");
errorMessage.innerHTML = "You need to enter the identical values";
errorMessage.style = "color:red";

let submitButton = document.querySelector(".btn");
submitButton.addEventListener("click", (event) => {
	if ( inputPassword[0].value === inputPassword[1].value ) {
		errorMessage.remove();
		alert("You are welcome");
	} else {
		document.querySelector(".password-form").appendChild(errorMessage);
	}
})