function createNewUser() {
  this.firstName = prompt( "Enter your name", "" );
  this.lastName = prompt( "Enter your surname", "" );
  this.birthday = prompt( "Enter your birthday", "" );
}

let newUser = new createNewUser();

newUser.getLogin = function() {
  return ( newUser.firstName[0] + newUser.lastName ).toLowerCase();
};

newUser.getAge = function() {
	let currentDate = new Date;
	let userBirthDay = new Date(newUser.birthday);
  let age = currentDate.getFullYear() - userBirthDay.getFullYear();
  if ( currentDate.getMonth() - userBirthDay.getMonth() < 0 ) {
    age--;
  } else if ( currentDate.getMonth() - userBirthDay.getMonth() === 0 ) {
    if ( currentDate.getDate() - userBirthDay.getDate() < 0 ) {
      age--;
    }
  }
  return age; 
};

newUser.getPassword = function() {
	let userBirthDay = new Date(newUser.birthday);
	return ( newUser.firstName[0].toUpperCase() + newUser.lastName.toLowerCase() + userBirthDay.getFullYear() );
};

console.log( newUser.getLogin() );
console.log( newUser );
console.log( newUser.getAge() );
console.log( newUser.getPassword() );