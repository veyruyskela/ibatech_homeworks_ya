$(".tabs-title").on("click", function() {
	$(".tabs-title").removeClass("active");
	$(this).addClass("active");

	$(".list-item").css("display", "none");

	let atribut = $(this).attr("data-type");

	let a = $(".list-item").map(function() {
		return $(this).attr("data-type");
	}).get();

	$(".list-item").each(function(i) {
		if( a[i] === atribut ) {
			$(this).css("display", "list-item");
		}
	});
});