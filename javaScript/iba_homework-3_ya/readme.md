Functions allows us to execute some pieces of code without repeating the code itself.

When the function is called, the arguments (parameters) pass to the body of the function.
And the function, which has been called, gets the parameters, begins to execute, and it is dealing with these parameters. Like functions in math, functions in programming depend on their arguments.