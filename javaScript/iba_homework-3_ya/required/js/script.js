let firstNumber = +prompt( "Enter the first number" );
let secondNumber = +prompt( "Enter the second number" );
let operation = prompt( "Enter the operation" );
/* the main functionality */
/* first way */
function myCalculator ( firstNumber, secondNumber, operation ) {
	if ( operation === "+" ) {
		console.log ( firstNumber + secondNumber );
	} else if ( operation === "-" ) {
		console.log ( firstNumber - secondNumber );
	} else if ( operation === "*" ) {
		console.log ( firstNumber * secondNumber );
	} else if ( operation === "/" ) {
		console.log ( firstNumber / secondNumber );
	}
}

myCalculator ( firstNumber, secondNumber, operation );
/* second way */
/*
function myCalculator ( firstNumber, secondNumber, operation ) {
	if ( operation === "+" ) {
		return ( firstNumber + secondNumber );
	} else if ( operation === "-" ) {
		return ( firstNumber - secondNumber );
	} else if ( operation === "*" ) {
		return ( firstNumber * secondNumber );
	} else if ( operation === "/" ) {
		return ( firstNumber / secondNumber );
	}
}

console.log ( myCalculator ( firstNumber, secondNumber, operation ) );
*/